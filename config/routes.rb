  Rails.application.routes.draw do
  get 'store/index'
  get 'order_products/create'

  get 'order_products/update'

  get 'order_products/destroy'
  get 'orders/new'

  get 'carts/show'


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
   mount Ckeditor::Engine => '/ckeditor'
  root 'homes#index'
  get '/home', to: 'homes#index'
  get '/payment', to: 'products#payment'
  get '/products', to: 'products#index'
  get '/designs', to: 'designs#index'
  get '/bedroom_boy', to: 'designs#bedroom_boy'
  get '/bedroom_girl', to: 'designs#bedroom_girl'
  get '/kitchen', to: 'designs#kitchen'
  get '/living_room', to: 'designs#living_room'
  get '/apartment', to: 'designs#apartment'
  get '/villa', to: 'designs#villa'
  get '/abouts', to: 'abouts#index'
  get '/helps' , to: 'helps#index'
  get '/contacts' , to: 'contacts#contact'

  resource :cart, only: [:show]
  resources :order_products, only: [:show, :create, :update, :destroy]

  resources :orders
  resources :contacts
 resources :products, only: [:index, :show]

  devise_for :users, controllers: { registration: "registration" }

  # get ':controller(/:action(/:id(.:format)))'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
