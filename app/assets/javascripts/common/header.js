$(document).on('ready turbolinks:load', function() {
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
    $(function(){
        $('li a').click(function(){
            $('li a').removeClass();
            $(this).addClass('active');
        });
    });
});
