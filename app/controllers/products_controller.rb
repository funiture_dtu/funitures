class ProductsController < ApplicationController
 before_action :load_product, only: :show
  before_action :load_all
  before_action :load_brand_and_style_and_collection
  def index
    @params = params.reject { |k| ["action","controller"].include?(k.to_s) }
    brand_id = params[:brand_id]
    style_id = params[:style_id]
    collection_id = params[:collection_id]
    @order_product = current_order.order_products.new
    if params[:search].present?
      @products = Product.search(params[:search])
    else
      @products = Product.all
    end
   @products = @products.where(brand_id: brand_id) if brand_id.present?
   @products = @products.where(style_id: style_id) if style_id.present?
   @products = @products.where(collection_id: collection_id) if collection_id.present?
   @products = @products.order("created_at DESC").page(params[:page]).per(9)
   @products = @products.order("created_at DESC").page(params[:page]).per(9)
   @products = @products.order("created_at DESC").page(params[:page]).per(9)
  end
  
  def show
  end
  
  def load_product
    @product = Product.find_by(id: params[:id]) 
    @pre_product = Product.find_by(id: params[:pre_product_id])
    # Create cart for user
    @order_product = current_order.order_products.new
    # count weight for product
    if @pre_product != @product
      @pre_product.goto ||= {}
      @hash = @pre_product.goto
      if @pre_product.goto[params[:id]].nil?
        @pre_product.goto[params[:id]] = 1
        else @hash.keys.each do |k|
            if k.to_i == @product.id
              @last_weight = @hash[k].to_i
              @hash[k] = @hash[k].to_i + 1
            end
        end
      end
      @pre_product.save
    end
    if @product.goto.nil?
      @hot_products = []
      @allIdProduct = Product.all.ids
      @newIds = @allIdProduct - [@product.id]
      if @newIds.length > 4 
        @randomIds = @newIds.sample(5)
      else 
        @randomIds = @newIds.sample(@newIds.length)
      end
    @randomIds.each do |id|
      @hot_products << Product.find(id)
    end
   else
      @weight_products = @product.goto.length
      @random_variable = 2  
      @hot_products = Product.where(id: @product.goto.keys) 
        if (@weight_products > 2)
          @new_product = @product.goto.sort_by {|key, value| value}.reverse
          @new_product = @new_product[0..2]
        else 
          @random_variable = 5 - @weight_products
          @new_product = @product.goto.sort_by {|key, value| value}.reverse
        end
      @ids = @new_product.collect {|key, value| key.to_i}
      @products = []
      @ids.each do |id| 
         @products << Product.find(id)
      end
      @allIdProduct = Product.all.ids
      if @allIdProduct.length < @random_variable + @ids.length 
        @random_variable = @allIdProduct.length - @ids.length
      end
      @newIds = @allIdProduct - @ids - [@product.id]
      @randomIds = @newIds.sample(@random_variable)
      @randomIds.each do |id|
        @products << Product.find(id)
      end
      @hot_products = @products
    end
  end
  private
    def load_product_by_brand
      @brand = Brand.find_by id: params[:brand_id]
      if @brand
        @products = @brand.products.page(params[:page]).per(9)

      end
    end

    def load_product_by_style
      @style = Style.find_by id: params[:style_id]
      if @style
        @products = @style.products.page(params[:page]).per(9)
      end
    end

    def load_product_by_collection
      @collection = Collection.find_by id: params[:collection_id]
      if @collection
        @products = @collection.products.page(params[:page]).per(9)
      end
    end

    def load_all
      if @brand.nil? && @style.nil? && @collection.nil?
        @products = Product.page(params[:page]).per 12
      end
    end

    def load_brand_and_style_and_collection
      @style = Style.all
      @brand = Brand.all
      @collection = Collection.all
    end

    def products_params
      params.require(:products).permit(:name, :description, :price, :status, :image)
    end
end