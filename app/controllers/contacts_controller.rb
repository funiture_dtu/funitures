class ContactsController < ApplicationController
before_action :load_contact
def contact
	@contact = Contact.new
end

def create
	
end

private

	def contact_params
	params.require(:contact).permit :name, :email, :phone, :message
	end 

	def load_contact
	@contact = Contact.find_by id: params[:id]
	end

end