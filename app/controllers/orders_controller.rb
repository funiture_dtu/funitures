class OrdersController < ApplicationController
  before_action :new

  def new
    @order = Order.new
  end

  def create
    @name = order_params[:name]
    @address = order_params[:address]
    @email = order_params[:email]
    @phone = order_params[:phone]
    @pay_type = order_params[:pay_type]

    if @name == "" || @address == "" || @email == "" || @phone == "" || @pay_type == ""
      flash[:error] = "Error. Please retype"
      redirect_to new_order_path
    else
      @order.name = @name
      @order.address = @address
      @order.email = @email
      @order.phone = @phone
      @order.pay_type = @pay_type
      @order.save
      redirect_to products_path
    end
  end

  def edit
  end
 
  private

  def order_params
    params.require(:order).permit :name, :address, :email, :phone, :pay_type
  end 

  def load_order
    @order = Order.find_by id: params[:id]
  end

end