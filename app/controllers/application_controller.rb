class ApplicationController < ActionController::Base
  before_action :isAdmin
  before_action :load_product_by_brand
  before_action :load_product_by_style
  before_action :load_product_by_collection
  before_action :load_brand_and_style_and_collection
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "Access denied. You are not authorized to access the requested page."
    redirect_to root_path :alert => exception.message
  end

  protected
  # lấy ra tên model từ controller ví dụ: UsersController sẽ lấy ra được là User
  class << self
    def permission
      return name = self.name.gsub("Controller","").singularize.split("::").last.constantize.name rescue nil
    end
  end

  def current_ability
    @current_ability ||= Ability.new current_user
  end

  def isAdmin
   if (params[:controller] != 'devise/sessions' || params[:action] != 'destroy') &&(params[:controller] != 'products' || params[:action] != 'home') && current_user.present? && current_user.isAdmin
     redirect_to '/admin'
   end

  end

  # lấy ra tất cả các permissions của user
  def load_permissions
    @current_permissions = current_user.role.permissions.collect{|p| [p.subject_class, p.action]}
  end

  def index
    
  end

  def show
    @products = Product.find(params[:id])
    respond_to do |format|
     format.html { render :text => @products.index }
   end
 end

private
  def load_product_by_brand
    @brand = Brand.find_by id: params[:brand_id]  
  end

  def load_product_by_style
    @style = Style.find_by id: params[:style_id]
  end

  def load_product_by_collection
    @collection = Collection.find_by id: params[:collection_id]
  end

  def load_brand_and_style_and_collection
    @style = Style.all
    @brand = Brand.all
    @collection = Collection.all
  end

  def products_params
    params.require(:products).permit(:name, :price, :status, :image)
  end

  helper_method :current_order
  def current_order
    if !session[:order_id].nil?
      Order.find(session[:order_id])
    else
      Order.new
    end
  end
end
