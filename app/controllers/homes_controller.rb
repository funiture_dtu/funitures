class HomesController < ApplicationController
	before_action :load_product 
	def index
		@products = Product.all
		@products = Product.limit(6)
		@collection = Collection.limit(3)
	end

	def show
		@products = Product.find(params[:id])
		@collection = Collection.find(params[:id])
		respond_to do |format|
			format.html { render :text => @products.index }
		end
	end

private
	def load_product
		@products = Product.find_by id: params[:product_id]
	end

	# def load_collection
	# 	@collection = Collection.find_by id: params[:collection_id]
	# end

	def homes_params
		params.require(:products).permit( :image)
	end
end