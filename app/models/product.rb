class Product < ActiveRecord::Base

	 has_attached_file :image, styles: { large: "600x600>", medium: "250x250>", thumb: "100x100>" }
   # ,
       #  :storage => :google_drive,
       # :google_drive_credentials => "#{Rails.root}/config/google_drive.yml",
       # :google_drive_options => {
       #     public_folder_id: '0B6QUwbBS7M0Ta2pudWMzeFZoRUk',
       #       :path => proc { |style| "#{style}_#{id}_#{image.original_filename}" }
       # }
    validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

    belongs_to :brand
    
    enum status: [:active, :archived]
    has_many :order_products

    default_scope { where(active: true) }
    def self.search(search)
         # Title is for the above case, the OP incorrectly had 'name'
         where("name LIKE ?", "%#{search}%")
    end

end
