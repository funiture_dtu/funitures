# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


OrderStatus.delete_all
OrderStatus.create!  name: "In Progress"
OrderStatus.create!  name: "Placed"
OrderStatus.create!  name: "Shipped"
OrderStatus.create! name: "Cancelled"

Brand.delete_all
Brand.create! name: "Asfalio"
Brand.create!  name: "Flenefor"
Brand.create!  name: "Garbini"
Brand.create! name: "Opaquio"
Brand.create!  name: "Varadini"

Product.delete_all
Product.create!  name: "Living room", price: 0.49, active: true
Product.create!  name: "Bedroom", price: 0.29, active: true
Product.create!  name: "Dining room", price: 1.99, active: true
Product.create!  name: "Diningroom", price: 4.99, active: true
Product.create!  name: "Product a", price: 4.99, active: true
Product.create!  name: "Product b", price: 4.99, active: true
Product.create!  name: "Product c", price: 4.99, active: true
Product.create!  name: "Product d", price: 4.99, active: true
Product.create!  name: "Product 1", price: 4.99, active: true
Product.create!  name: "Product 2", price: 4.99, active: true

Style.delete_all
Style.create!  name: "Classic"
Style.create!  name: "Contemporany"
Style.create!  name: "New Age"
Style.create!  name: "Rustic"

Collection.delete_all
Collection.create!  name: "Bed Room"
Collection.create!  name: "Living Room"
Collection.create!  name: "Dining Room"

User.delete_all
User.create! email: "dovuquan@gmail.com", password: "dovuquan", password_confirmation: "dovuquan", isAdmin: true
